package ru.t1.zkovalenko.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_dto_user")
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(unique = true, nullable = false)
    private String login;

    @NotNull
    @Column(nullable = false, name = "password")
    private String passwordHash;

    @NotNull
    @Column
    private String email = "";

    @NotNull
    @Column(name = "first_name")
    private String firstName = "";

    @NotNull
    @Column(name = "last_name")
    private String lastName = "";

    @NotNull
    @Column(name = "middle_name")
    private String middleName = "";

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(nullable = false, columnDefinition = "boolean default false", name = "locked_flg")
    private Boolean locked = false;

}
