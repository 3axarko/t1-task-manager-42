package ru.t1.zkovalenko.tm.dto.response.project;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class ProjectRemoveByIndexResponse extends AbstractProjectResponse {
}
