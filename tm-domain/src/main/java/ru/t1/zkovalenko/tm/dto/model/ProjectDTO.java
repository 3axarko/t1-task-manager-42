package ru.t1.zkovalenko.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.model.IWBS;
import ru.t1.zkovalenko.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_dto_project")
public final class ProjectDTO extends AbstractUserOwnerModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @NotNull
    @Column(length = 2000)
    private String description = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    private Date created = new Date();

    public ProjectDTO(@NotNull String name) {
        this.name = name;
    }

    public ProjectDTO(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public ProjectDTO(@NotNull String name, @NotNull Status status) {
        this.name = name;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
