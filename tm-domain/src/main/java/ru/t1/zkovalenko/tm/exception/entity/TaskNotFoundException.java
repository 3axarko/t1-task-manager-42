package ru.t1.zkovalenko.tm.exception.entity;

public final class TaskNotFoundException extends AbstractEntityException {

    public TaskNotFoundException() {
        super("Task not Found");
    }

}
