package ru.t1.zkovalenko.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractUserOwnerModel extends AbstractModel {

    //    @Nullable
//    @Column(nullable = false, name = "user_id")
//    private String userId;
    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;

}
