package ru.t1.zkovalenko.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.model.TaskDTO;

public final class TaskTestData {

    @NotNull
    public final static String TASK_NAME = "TASK_NAME";

    @NotNull
    public final static String TASK_DESCRIPTION = "TASK_DESCRIPTION";

    @NotNull
    public final static TaskDTO TASK1 = new TaskDTO();

    @NotNull
    public final static TaskDTO TASK2 = new TaskDTO();

    {
        TASK1.setName(TASK_NAME);
        TASK1.setDescription(TASK_DESCRIPTION);
    }

}
