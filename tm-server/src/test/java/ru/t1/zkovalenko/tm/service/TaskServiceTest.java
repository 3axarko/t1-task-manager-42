package ru.t1.zkovalenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.api.service.ITaskService;
import ru.t1.zkovalenko.tm.dto.model.TaskDTO;
import ru.t1.zkovalenko.tm.marker.UnitCategory;
import ru.t1.zkovalenko.tm.marker.UnitServiceCategory;

import static ru.t1.zkovalenko.tm.constant.TaskTestData.*;
import static ru.t1.zkovalenko.tm.constant.UserTestData.USER1;
import static ru.t1.zkovalenko.tm.enumerated.Status.COMPLETED;
import static ru.t1.zkovalenko.tm.enumerated.Status.NOT_STARTED;

@Category({UnitCategory.class, UnitServiceCategory.class})
public final class TaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Nullable
    private TaskDTO taskCreated;

    @After
    public void after() {
        if (taskCreated != null) taskService.remove(taskCreated);
        taskCreated = null;
    }

    @Test
    public void changeTaskStatusById() {
        taskCreated = taskService.create(USER1.getId(), TASK1.getId());
        Assert.assertEquals(taskCreated.getStatus(), NOT_STARTED);
        @NotNull final TaskDTO taskChanged = taskService
                .changeTaskStatusById(USER1.getId(), taskCreated.getId(), COMPLETED);
        Assert.assertEquals(taskChanged.getStatus(), COMPLETED);
    }

    @Test
    public void changeTaskStatusByIndex() {
        taskCreated = taskService.create(USER1.getId(), TASK1.getId());
        Assert.assertEquals(taskCreated.getStatus(), NOT_STARTED);
        @NotNull final TaskDTO taskChanged = taskService
                .changeTaskStatusByIndex(USER1.getId(), 1, COMPLETED);
        Assert.assertEquals(taskChanged.getStatus(), COMPLETED);
    }

    @Test
    public void createUserIdName() {
        taskCreated = taskService.create(USER1.getId(), TASK_NAME);
        @NotNull final TaskDTO taskFounded = taskService.findOneById(taskCreated.getId());
        Assert.assertEquals(TASK_NAME, taskFounded.getName());
    }

    @Test
    public void createUserIdNameDescription() {
        taskCreated = taskService.create(USER1.getId(), TASK_NAME, TASK_DESCRIPTION);
        @NotNull final TaskDTO taskFounded = taskService.findOneById(taskCreated.getId());
        Assert.assertEquals(TASK_NAME, taskFounded.getName());
        Assert.assertEquals(TASK_DESCRIPTION, taskFounded.getDescription());
    }

    @Test
    public void updateById() {
        taskCreated = taskService.create(USER1.getId(), TASK_NAME, TASK_DESCRIPTION);
        taskService.updateById(USER1.getId(),
                taskCreated.getId(),
                TASK_NAME + "1",
                TASK_DESCRIPTION + "1");
        @NotNull final TaskDTO taskFounded = taskService.findOneById(taskCreated.getId());
        Assert.assertEquals(taskFounded.getName(), TASK_NAME + "1");
        Assert.assertEquals(taskFounded.getDescription(), TASK_DESCRIPTION + "1");
    }

    @Test
    public void updateByIndex() {
        taskCreated = taskService.create(USER1.getId(), TASK_NAME, TASK_DESCRIPTION);
        @NotNull final TaskDTO taskFounded = taskService.findOneByIndex(1);
        @NotNull final TaskDTO taskChanged = taskService.updateByIndex(taskFounded.getUserId(),
                1,
                TASK_NAME + "1",
                TASK_DESCRIPTION + "1");
        Assert.assertEquals(taskChanged.getName(), TASK_NAME + "1");
        Assert.assertEquals(taskChanged.getDescription(), TASK_DESCRIPTION + "1");
    }

}
