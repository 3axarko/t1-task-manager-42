package ru.t1.zkovalenko.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.model.ProjectDTO;

public final class ProjectTestData {

    @NotNull
    public final static String PROJECT_NAME = "PROJECT_NAME";

    @NotNull
    public final static String PROJECT_DESCRIPTION = "PROJECT_DESCRIPTION";

    @NotNull
    public final static ProjectDTO PROJECT1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO PROJECT2 = new ProjectDTO();

    {
        PROJECT1.setName(PROJECT_NAME);
        PROJECT1.setDescription(PROJECT_DESCRIPTION);
    }

}
