package ru.t1.zkovalenko.tm.api.service;

import ru.t1.zkovalenko.tm.dto.model.SessionDTO;

public interface ISessionService extends IUserOwnerService<SessionDTO> {

}
